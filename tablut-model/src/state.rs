use nom::IResult;
use nom::{bytes::complete::tag, character::complete::alphanumeric0, multi::separated_list0};
use std::collections::VecDeque;

use crate::board::BLACK_NORTH_CAMPS_POSITION;
use crate::board::BLACK_SOUTH_CAMPS_POSITION;
use crate::board::BLACK_WEST_CAMPS_POSITION;
use crate::board::CASTLE_POSITION;
use crate::board::KING_SPECIAL_CELLS;
use crate::board::KING_SURROUNDED_ABOVE_CELLS;
use crate::board::KING_SURROUNDED_BELOW_CELLS;
use crate::board::KING_SURROUNDED_CELLS;
use crate::board::KING_SURROUNDED_LEFT_CELLS;
use crate::board::KING_SURROUNDED_RIGHT_CELLS;
use crate::board::SIDE_LENGTH;
use crate::board::STARTING_BLACK_POSITION;
use crate::board::STARTING_KING_POSITION;
use crate::board::STARTING_WHITE_POSITION;
use crate::board::{black_pawn_is_in_east_camp, OBSTACLES_CELLS};
use crate::board::{
    black_pawn_is_in_north_camp, first_cell_for_column, first_cell_for_row, last_cell_for_column,
    last_cell_for_row, one_cell_up, BLACK_CAMPS_POSITION,
};
use crate::board::{black_pawn_is_in_south_camp, one_cell_down};
use crate::board::{black_pawn_is_in_west_camp, one_cell_left};
use crate::board::{one_cell_right, EnumeratingBoardPositionType};
use crate::board::{BLACK_EAST_CAMPS_POSITION, SAFE_CELL_POSITION};

use crate::model::{FenErrors, Move, MoveApplicationError, Turn};

// Reppresents the state for the game
#[derive(Eq, PartialEq, Debug, Clone)]
pub struct TablutState {
    // Position of the king inside the borad
    king_pos: EnumeratingBoardPositionType,
    // Position of white pawns
    white_pos: EnumeratingBoardPositionType,
    // Position of black pawns
    black_pos: EnumeratingBoardPositionType,
    // Current turn
    turn: Turn,
}

impl TablutState {
    // Get initial state
    pub fn initial_state() -> Self {
        Self {
            king_pos: STARTING_KING_POSITION,
            white_pos: STARTING_WHITE_POSITION,
            black_pos: STARTING_BLACK_POSITION,
            turn: Turn::White,
        }
    }

    // Gets all moves for some player
    pub fn moves_for_player(&self) -> Option<Vec<Move>> {
        match self.turn {
            Turn::White | Turn::Black => Some(
                if self.turn == Turn::White {
                    self.white_positions()
                } else {
                    self.black_positions()
                }
                .iter_ones()
                .map(|idx| self.moves_for_piece(idx))
                .flatten()
                .collect(),
            ),
            Turn::WhiteWon | Turn::BlackWon | Turn::Draw => None,
        }
    }

    // Gets all moves for a piece
    fn moves_for_piece(&self, pawn_position: usize) -> Vec<Move> {
        // 16 is the maximum amount of moves we can possibly have:
        // it ensures no further allocations are needed
        let mut moves = Vec::with_capacity(16);

        // All cells with something on it are forbidden
        let mut forbidden_cells = self.all_positions();

        // The cell for which we are computing moves must be checked as a piece
        debug_assert!(forbidden_cells[pawn_position]);

        // The castle cell is always forbidden too
        forbidden_cells |= CASTLE_POSITION;

        // Camps may or may not be forbidden to step on
        if self.turn == Turn::White {
            // Whites can never go on the camps
            forbidden_cells |= BLACK_CAMPS_POSITION;
        } else {
            // If the black pawn is not in a camp then all camps are banned, otherwise only the other camps are forbidden
            if black_pawn_is_in_west_camp(pawn_position) {
                forbidden_cells |= BLACK_NORTH_CAMPS_POSITION
                    | BLACK_EAST_CAMPS_POSITION
                    | BLACK_SOUTH_CAMPS_POSITION;
            } else if black_pawn_is_in_north_camp(pawn_position) {
                forbidden_cells |= BLACK_WEST_CAMPS_POSITION
                    | BLACK_EAST_CAMPS_POSITION
                    | BLACK_SOUTH_CAMPS_POSITION;
            } else if black_pawn_is_in_east_camp(pawn_position) {
                forbidden_cells |= BLACK_WEST_CAMPS_POSITION
                    | BLACK_NORTH_CAMPS_POSITION
                    | BLACK_SOUTH_CAMPS_POSITION;
            } else if black_pawn_is_in_south_camp(pawn_position) {
                forbidden_cells |= BLACK_WEST_CAMPS_POSITION
                    | BLACK_NORTH_CAMPS_POSITION
                    | BLACK_EAST_CAMPS_POSITION;
            } else {
                forbidden_cells |= BLACK_CAMPS_POSITION;
            }
        }

        // Check for moves
        // Up
        if let Some(end) = one_cell_up(pawn_position) {
            let start = first_cell_for_column(pawn_position);

            for cell in (start..=end)
                .rev()
                .step_by(SIDE_LENGTH)
                .take_while(|c| !forbidden_cells[*c])
            {
                moves.push(Move::new(pawn_position, cell));
            }
        }

        // Down
        if let Some(start) = one_cell_down(pawn_position) {
            let end = last_cell_for_column(pawn_position);

            for cell in (start..=end)
                .step_by(SIDE_LENGTH)
                .take_while(|c| !forbidden_cells[*c])
            {
                moves.push(Move::new(pawn_position, cell));
            }
        }

        // Left
        if let Some(end) = one_cell_left(pawn_position) {
            let start = first_cell_for_row(pawn_position);

            for cell in (start..=end).rev().take_while(|c| !forbidden_cells[*c]) {
                moves.push(Move::new(pawn_position, cell));
            }
        }

        // right
        if let Some(start) = one_cell_right(pawn_position) {
            let end = last_cell_for_row(pawn_position);

            for cell in (start..=end).take_while(|c| !forbidden_cells[*c]) {
                moves.push(Move::new(pawn_position, cell));
            }
        }

        moves
    }

    // Applies a move to the current state to get a new state
    pub fn apply_move(&self, m: &Move) -> Result<Self, MoveApplicationError> {
        let mut new_state = self.clone();

        debug_assert!(!(new_state.white_positions()[m.to] || new_state.black_positions()[m.to]));
        debug_assert!(new_state.white_positions()[m.from] || new_state.black_positions()[m.from]);

        if self.turn == Turn::White && (self.white_pos[m.from] || self.king_pos[m.from]) {
            let mut moving_king = false;
            if self.white_pos[m.from] {
                new_state.white_pos.set(m.from, false);
                new_state.white_pos.set(m.to, true);
            } else {
                new_state.king_pos.set(m.from, false);
                new_state.king_pos.set(m.to, true);

                moving_king = true;
            }

            // If we are playing white all captures are black
            let white_captures_normalized = !self.get_captures_for_white(m);
            new_state.black_pos &= white_captures_normalized;

            if (moving_king && (new_state.king_position() & SAFE_CELL_POSITION).count_ones() > 0)
                || new_state.black_positions().count_ones() == 0
            {
                // Whites have won
                new_state.turn = Turn::WhiteWon;
            } else {
                new_state.turn = Turn::Black;
            }

            Ok(new_state)
        } else if self.turn == Turn::Black && self.black_pos[m.from] {
            new_state.black_pos.set(m.from, false);
            new_state.black_pos.set(m.to, true);

            // If we are playing black all captures are white
            let black_captures_normalized = !self.get_captures_for_black(m);
            new_state.white_pos &= black_captures_normalized;
            new_state.king_pos &= black_captures_normalized;

            if new_state.king_position().count_ones() == 0
                || new_state.white_positions().count_ones() == 0
            {
                // Whites have won
                new_state.turn = Turn::BlackWon;
            } else {
                new_state.turn = Turn::White;
            }

            Ok(new_state)
        } else {
            Err(MoveApplicationError::WrongTurn)
        }
    }

    // Gets all captures for whites
    fn get_captures_for_white(&self, m: &Move) -> EnumeratingBoardPositionType {
        debug_assert!(!self.white_positions()[m.to]);
        debug_assert!(self.white_positions()[m.from]);

        let blacks = self.black_positions();
        let mut whites = self.white_pawns_positions();
        let king = self.king_position();
        whites |= king;

        whites.set(m.from, false);
        whites.set(m.to, true);

        self.get_normal_captures(m.to, &whites, &blacks)
    }

    // Gets all captures for blacks
    fn get_captures_for_black(&self, m: &Move) -> EnumeratingBoardPositionType {
        debug_assert!(!self.black_positions()[m.to]);
        debug_assert!(self.black_positions()[m.from]);

        let mut blacks = self.black_positions();
        blacks.set(m.from, false);
        blacks.set(m.to, true);
        let king = self.king_position();
        let mut whites = self.white_pawns_positions();

        let mut special_captures = EnumeratingBoardPositionType::ZERO;

        // Check if the king needs a special capture
        if (king & KING_SPECIAL_CELLS).count_ones() > 0 {
            if ((king & CASTLE_POSITION).count_ones() > 0
                && (blacks & KING_SURROUNDED_CELLS).count_ones() == 4)
                || (blacks & KING_SURROUNDED_ABOVE_CELLS).count_ones() == 3
                || (blacks & KING_SURROUNDED_BELOW_CELLS).count_ones() == 3
                || (blacks & KING_SURROUNDED_RIGHT_CELLS).count_ones() == 3
                || (blacks & KING_SURROUNDED_LEFT_CELLS).count_ones() == 3
            {
                special_captures |= king;
            }
        } else {
            whites |= king;
        }

        // Normal captures
        let mut captures = self.get_normal_captures(m.to, &blacks, &whites);
        captures |= special_captures;

        captures
    }

    /// Gets all non special captures
    fn get_normal_captures(
        &self,
        position: usize,
        attack: &EnumeratingBoardPositionType,
        defense: &EnumeratingBoardPositionType,
    ) -> EnumeratingBoardPositionType {
        let mut captured = EnumeratingBoardPositionType::ZERO;

        // Check UP
        if let Some(checked) = self.check(position, defense, attack, one_cell_up) {
            captured.set(checked, true);
        }

        // Check LEFT
        if let Some(checked) = self.check(position, defense, attack, one_cell_left) {
            captured.set(checked, true);
        }

        //Check DOWN
        if let Some(checked) = self.check(position, defense, attack, one_cell_down) {
            captured.set(checked, true);
        }

        //Check RIGHT
        if let Some(checked) = self.check(position, defense, attack, one_cell_right) {
            captured.set(checked, true);
        }

        captured
    }

    // Checks a move returning the idx of the captured pawn
    #[inline]
    fn check(
        &self,
        pos: usize,
        defense: &EnumeratingBoardPositionType,
        attack: &EnumeratingBoardPositionType,
        fn_dir: impl Fn(usize) -> Option<usize>,
    ) -> Option<usize> {
        // If there is a cell one cell away in the direction specified
        if let Some(cell_moved_once) = fn_dir(pos) {
            // If there is a cell two cells away in the direction specified
            if let Some(cell_moved_twice) = fn_dir(cell_moved_once) {
                // If the cell one cell away has a defense pawn to be captured
                // and on the other side we have an attacker or an obstacle
                if defense[cell_moved_once]
                    && (attack[cell_moved_twice] || OBSTACLES_CELLS[cell_moved_twice])
                {
                    return Some(cell_moved_once);
                }
            }
        }
        None
    }

    // Gets white piece positions
    #[inline]
    pub fn white_positions(&self) -> EnumeratingBoardPositionType {
        self.white_pos | self.king_pos
    }

    // Gets white pawns positions
    #[inline]
    pub fn white_pawns_positions(&self) -> EnumeratingBoardPositionType {
        self.white_pos
    }

    // Gets king positions
    #[inline]
    pub fn king_position(&self) -> EnumeratingBoardPositionType {
        self.king_pos
    }

    // Gets black pieces positions
    #[inline]
    pub fn black_positions(&self) -> EnumeratingBoardPositionType {
        self.black_pawns_positions()
    }

    // Gets black pawns positions
    #[inline]
    pub fn black_pawns_positions(&self) -> EnumeratingBoardPositionType {
        self.black_pos
    }

    // Gets current turn
    pub fn turn(&self) -> Turn {
        self.turn
    }

    // Gets all pieces position
    #[inline]
    pub fn all_positions(&self) -> EnumeratingBoardPositionType {
        self.white_positions() | self.black_positions()
    }

    // Parses a fen string to get a board
    fn parse_fen_string(s: &str) -> IResult<&str, Vec<&str>> {
        let (res, data) = separated_list0(tag("/"), alphanumeric0)(s)?;

        Ok((res, data))
    }

    // Gets a state from FEN representation
    pub fn from_fen(value: &str) -> Result<Self, FenErrors> {
        if value.is_empty() {
            return Err(FenErrors::EmptySource);
        }

        let parsed = Self::parse_fen_string(value);

        // Seems that current implementation cannot terminate with an error
        // if parsed.is_err() {
        //     return Err(FenErrors::InvalidParse);
        // }

        let (_, data) = parsed.unwrap();

        if data.len() != SIDE_LENGTH + 1 {
            return Err(FenErrors::InvalidNumberOfParts);
        }

        let invalid_board_data = data[..SIDE_LENGTH].iter().any(|p| {
            p.len() > SIDE_LENGTH
                || p.chars()
                    .any(|c| c != 'K' && c != 'P' && c != 'p' && !c.is_ascii_digit())
        });

        if invalid_board_data {
            return Err(FenErrors::InvalidBoardTag);
        }

        let mut blacks = EnumeratingBoardPositionType::ZERO;
        let mut whites = EnumeratingBoardPositionType::ZERO;
        let mut king = EnumeratingBoardPositionType::ZERO;

        for (row_idx, row) in data[..SIDE_LENGTH].iter().enumerate() {
            let mut col_idx = 0;
            for el in row.chars() {
                let pos = row_idx * SIDE_LENGTH + col_idx;
                match el {
                    'K' => king.set(pos, true),
                    'P' => whites.set(pos, true),
                    'p' => blacks.set(pos, true),
                    '1' | '2' | '3' | '4' | '5' | '6' | '7' | '8' | '9' => {
                        col_idx += (el.to_digit(10).unwrap() - 1) as usize
                    }
                    _ => return Err(FenErrors::InvalidBoardTag),
                }

                col_idx += 1;

                if col_idx > SIDE_LENGTH {
                    return Err(FenErrors::ColumnTooWide);
                }
            }
        }

        let turn = match data[SIDE_LENGTH] {
            "W" | "w" => Ok(Turn::White),
            "WW" | "ww" => Ok(Turn::WhiteWon),
            "B" | "b" => Ok(Turn::Black),
            "BW" | "bw" => Ok(Turn::BlackWon),
            "D" | "d" => Ok(Turn::Draw),
            _ => Err(FenErrors::InvalidTurn),
        }?;

        Ok(TablutState {
            king_pos: king,
            white_pos: whites,
            black_pos: blacks,
            turn,
        })
    }
}

#[cfg(test)]
mod tests {
    use crate::model::PlayerSide;

    use super::*;

    #[test]
    fn test_new_state() {
        let init = TablutState::initial_state();

        assert_eq!(init.king_position(), STARTING_KING_POSITION);
        assert_eq!(init.black_pawns_positions(), STARTING_BLACK_POSITION);
        assert_eq!(init.black_positions(), STARTING_BLACK_POSITION);
        assert_eq!(init.white_pawns_positions(), STARTING_WHITE_POSITION);
        assert_eq!(
            init.white_positions(),
            STARTING_WHITE_POSITION | STARTING_KING_POSITION
        );
        assert_eq!(
            init.all_positions(),
            STARTING_WHITE_POSITION | STARTING_KING_POSITION | STARTING_BLACK_POSITION
        );
    }

    #[test]
    fn test_fen_1() {
        let parsed = TablutState::from_fen("3ppp3/4p4/4P4/p3P3p/ppPPKPPpp/p3P3p/4P4/4p4/3ppp3/w");
        let expected = TablutState::initial_state();
        assert!(parsed.is_ok());
        assert_eq!(expected, parsed.unwrap());
    }

    #[test]
    fn test_fen_2() {
        let parsed = TablutState::from_fen("3ppp3/4p4/4P4/p3P3p/ppPPKPPpp/p3P3p/4P4/4p4/3ppp3/w");
        assert!(parsed.is_ok());
        assert_eq!(Turn::White, parsed.unwrap().turn);
    }

    #[test]
    fn test_fen_3() {
        let parsed = TablutState::from_fen("3ppp3/4p4/4P4/p3P3p/ppPPKPPpp/p3P3p/4P4/4p4/3ppp3/b");
        assert!(parsed.is_ok());
        assert_eq!(Turn::Black, parsed.unwrap().turn);
    }

    #[test]
    fn test_fen_4() {
        let parsed = TablutState::from_fen("3ppp3/4p4/4P4/p3P3p/ppPPKPPpp/p3P3p/4P4/4p4/3ppp3/ww");
        assert!(parsed.is_ok());
        assert_eq!(Turn::WhiteWon, parsed.unwrap().turn);
    }

    #[test]
    fn test_fen_5() {
        let parsed = TablutState::from_fen("3ppp3/4p4/4P4/p3P3p/ppPPKPPpp/p3P3p/4P4/4p4/3ppp3/bw");
        assert!(parsed.is_ok());
        assert_eq!(Turn::BlackWon, parsed.unwrap().turn);
    }

    #[test]
    fn test_fen_6() {
        let parsed = TablutState::from_fen("3ppp3/4p4/4P4/p3P3p/ppPPKPPpp/p3P3p/4P4/4p4/3ppp3/d");
        assert!(parsed.is_ok());
        assert_eq!(Turn::Draw, parsed.unwrap().turn);
    }

    #[test]
    fn test_fen_7() {
        let parsed = TablutState::from_fen("9/9/9/9/9/9/9/9/9/d");
        assert!(parsed.is_ok());
        assert_eq!(Turn::Draw, parsed.unwrap().turn);
    }

    #[test]
    fn test_fen_8() {
        let parsed = TablutState::from_fen("1K7/9/9/9/9/9/9/9/9/w");
        let mut expected = EnumeratingBoardPositionType::ZERO;
        expected.set(1, true);
        assert!(parsed.is_ok());
        assert_eq!(expected, parsed.unwrap().king_pos);
    }

    #[test]
    fn test_fen_9() {
        let parsed = TablutState::from_fen("9/9/9/9/9/9/9/9/9/tttt");
        assert!(parsed.is_err());
        assert_eq!(FenErrors::InvalidTurn, parsed.unwrap_err());
    }

    #[test]
    fn test_fen_10() {
        let parsed = TablutState::from_fen("55/9/9/9/9/9/9/9/9/w");
        assert!(parsed.is_err());
        assert_eq!(FenErrors::ColumnTooWide, parsed.unwrap_err());
    }

    #[test]
    fn test_fen_11() {
        let parsed = TablutState::from_fen("1PP99/9/9/9/9/9/9/9/9/w");
        assert!(parsed.is_err());
        assert_eq!(FenErrors::ColumnTooWide, parsed.unwrap_err());
    }

    #[test]
    fn test_fen_12() {
        let parsed: Result<TablutState, FenErrors> = TablutState::from_fen("A/9/9/9/9/9/9/9/9/w");
        assert!(parsed.is_err());
        assert_eq!(FenErrors::InvalidBoardTag, parsed.unwrap_err());
    }

    #[test]
    fn test_fen_13() {
        let parsed: Result<TablutState, FenErrors> = TablutState::from_fen("0/9/9/9/9/9/9/9/9/w");
        assert!(parsed.is_err());
        assert_eq!(FenErrors::InvalidBoardTag, parsed.unwrap_err());
    }

    #[test]
    fn test_fen_14() {
        let parsed: Result<TablutState, FenErrors> = TablutState::from_fen("9/9/9/9/9/9/w");
        assert!(parsed.is_err());
        assert_eq!(FenErrors::InvalidNumberOfParts, parsed.unwrap_err());
    }

    #[test]
    fn test_fen_15() {
        let parsed: Result<TablutState, FenErrors> = TablutState::from_fen("");
        assert!(parsed.is_err());
        assert_eq!(FenErrors::EmptySource, parsed.unwrap_err());
    }

    #[test]
    fn test_fen_16() {
        let parsed: Result<TablutState, FenErrors> = TablutState::from_fen("???");
        assert!(parsed.is_err());
        assert_eq!(FenErrors::InvalidNumberOfParts, parsed.unwrap_err());
    }

    #[test]
    fn test_fen_17() {
        let parsed: Result<TablutState, FenErrors> = TablutState::from_fen("?/?/?/?/?/?/?/?/?/w");
        assert!(parsed.is_err());
        assert_eq!(FenErrors::InvalidNumberOfParts, parsed.unwrap_err());
    }

    fn apply_move_generation_test(initial_state: &str, pos: usize, moves_expected: &[Move]) {
        let state = TablutState::from_fen(initial_state).unwrap();

        let moves = state.moves_for_piece(pos);

        let moves_player = state.moves_for_player();
        assert!(moves_player.is_some());
        let moves_player = moves_player.unwrap();
        assert_eq!(moves, moves_player);

        assert_eq!(moves.len(), moves_expected.len());
        for exp in moves_expected {
            assert!(moves.contains(exp));
        }
    }

    #[test]
    fn test_move_generation_1() {
        apply_move_generation_test(
            "9/9/2P6/9/9/9/9/9/9/w",
            20,
            &[
                Move::new(20, 2),
                Move::new(20, 11),
                Move::new(20, 19),
                Move::new(20, 18),
                Move::new(20, 21),
                Move::new(20, 22),
                Move::new(20, 23),
                Move::new(20, 24),
                Move::new(20, 25),
                Move::new(20, 26),
                Move::new(20, 29),
                Move::new(20, 38),
                Move::new(20, 47),
                Move::new(20, 56),
                Move::new(20, 65),
                Move::new(20, 74),
            ],
        );
    }

    #[test]
    fn test_move_generation_2() {
        apply_move_generation_test(
            "9/9/1P7/9/9/9/9/9/9/w",
            19,
            &[
                Move::new(19, 1),
                Move::new(19, 10),
                Move::new(19, 18),
                Move::new(19, 20),
                Move::new(19, 21),
                Move::new(19, 22),
                Move::new(19, 23),
                Move::new(19, 24),
                Move::new(19, 25),
                Move::new(19, 26),
                Move::new(19, 28),
            ],
        );
    }

    #[test]
    fn test_move_generation_3() {
        apply_move_generation_test(
            "P8/9/9/9/9/9/9/9/9/w",
            0,
            &[
                Move::new(0, 1),
                Move::new(0, 2),
                Move::new(0, 9),
                Move::new(0, 18),
            ],
        );
    }

    #[test]
    fn test_move_generation_4() {
        apply_move_generation_test(
            "9/9/9/9/9/9/9/9/P8/w",
            72,
            &[
                Move::new(72, 63),
                Move::new(72, 54),
                Move::new(72, 74),
                Move::new(72, 73),
            ],
        );
    }

    #[test]
    fn test_move_generation_5() {
        apply_move_generation_test(
            "9/9/9/9/9/9/9/9/8P/w",
            80,
            &[
                Move::new(80, 79),
                Move::new(80, 78),
                Move::new(80, 71),
                Move::new(80, 62),
            ],
        );
    }

    #[test]
    fn test_move_generation_6() {
        apply_move_generation_test(
            "8P/9/9/9/9/9/9/9/9/w",
            8,
            &[
                Move::new(8, 7),
                Move::new(8, 6),
                Move::new(8, 17),
                Move::new(8, 26),
            ],
        );
    }

    #[test]
    fn test_move_generation_8() {
        apply_move_generation_test(
            "9/1P7/9/9/9/9/9/9/9/w",
            10,
            &[
                Move::new(10, 1),
                Move::new(10, 9),
                Move::new(10, 11),
                Move::new(10, 12),
                Move::new(10, 19),
                Move::new(10, 28),
            ],
        );
    }

    #[test]
    fn test_move_generation_9() {
        apply_move_generation_test(
            "9/9/9/9/9/9/9/7P1/9/w",
            70,
            &[
                Move::new(70, 71),
                Move::new(70, 69),
                Move::new(70, 68),
                Move::new(70, 79),
                Move::new(70, 61),
                Move::new(70, 52),
            ],
        );
    }

    #[test]
    fn test_move_generation_10() {
        apply_move_generation_test(
            "9/9/9/9/9/9/9/7P1/9/w",
            70,
            &[
                Move::new(70, 71),
                Move::new(70, 69),
                Move::new(70, 68),
                Move::new(70, 79),
                Move::new(70, 61),
                Move::new(70, 52),
            ],
        );
    }

    #[test]
    fn test_move_generation_11() {
        apply_move_generation_test(
            "4p4/9/9/9/9/9/9/9/9/b",
            4,
            &[
                Move::new(4, 0),
                Move::new(4, 1),
                Move::new(4, 2),
                Move::new(4, 3),
                Move::new(4, 5),
                Move::new(4, 6),
                Move::new(4, 7),
                Move::new(4, 8),
                Move::new(4, 13),
                Move::new(4, 22),
                Move::new(4, 31),
            ],
        );
    }

    #[test]
    fn test_move_generation_12() {
        apply_move_generation_test(
            "9/9/4p4/9/9/9/9/9/9/b",
            22,
            &[
                Move::new(22, 31),
                Move::new(22, 18),
                Move::new(22, 19),
                Move::new(22, 20),
                Move::new(22, 21),
                Move::new(22, 23),
                Move::new(22, 24),
                Move::new(22, 25),
                Move::new(22, 26),
            ],
        );
    }

    #[test]
    fn test_move_generation_13() {
        apply_move_generation_test(
            "9/9/9/9/p/9/9/9/9/b",
            36,
            &[
                Move::new(36, 0),
                Move::new(36, 9),
                Move::new(36, 18),
                Move::new(36, 27),
                Move::new(36, 45),
                Move::new(36, 54),
                Move::new(36, 63),
                Move::new(36, 72),
                Move::new(36, 37),
                Move::new(36, 38),
                Move::new(36, 39),
            ],
        );
    }

    #[test]
    fn test_move_generation_14() {
        apply_move_generation_test(
            "9/9/9/9/2p6/9/9/9/9/b",
            38,
            &[
                Move::new(38, 39),
                Move::new(38, 2),
                Move::new(38, 11),
                Move::new(38, 20),
                Move::new(38, 29),
                Move::new(38, 47),
                Move::new(38, 56),
                Move::new(38, 65),
                Move::new(38, 74),
            ],
        );
    }

    #[test]
    fn test_move_generation_15() {
        apply_move_generation_test(
            "9/9/9/9/8p/9/9/9/9/b",
            44,
            &[
                Move::new(44, 8),
                Move::new(44, 17),
                Move::new(44, 26),
                Move::new(44, 35),
                Move::new(44, 53),
                Move::new(44, 62),
                Move::new(44, 71),
                Move::new(44, 80),
                Move::new(44, 43),
                Move::new(44, 42),
                Move::new(44, 41),
            ],
        );
    }

    #[test]
    fn test_move_generation_16() {
        apply_move_generation_test(
            "9/9/9/9/6p2/9/9/9/9/b",
            42,
            &[
                Move::new(42, 41),
                Move::new(42, 6),
                Move::new(42, 15),
                Move::new(42, 24),
                Move::new(42, 33),
                Move::new(42, 51),
                Move::new(42, 60),
                Move::new(42, 69),
                Move::new(42, 78),
            ],
        );
    }

    #[test]
    fn test_move_generation_17() {
        apply_move_generation_test(
            "9/9/9/9/9/9/9/9/4p4/b",
            76,
            &[
                Move::new(76, 72),
                Move::new(76, 73),
                Move::new(76, 74),
                Move::new(76, 75),
                Move::new(76, 77),
                Move::new(76, 78),
                Move::new(76, 79),
                Move::new(76, 80),
                Move::new(76, 67),
                Move::new(76, 58),
                Move::new(76, 49),
            ],
        );
    }

    #[test]
    fn test_move_generation_18() {
        apply_move_generation_test(
            "9/9/9/9/9/9/4p4/9/9/b",
            58,
            &[
                Move::new(58, 49),
                Move::new(58, 54),
                Move::new(58, 55),
                Move::new(58, 56),
                Move::new(58, 57),
                Move::new(58, 59),
                Move::new(58, 60),
                Move::new(58, 61),
                Move::new(58, 62),
            ],
        );
    }

    #[test]
    fn test_move_generation_19() {
        let state = TablutState::from_fen("9/9/9/9/9/9/4p4/9/9/d").unwrap();

        let moves = state.moves_for_player();
        assert!(moves.is_none());
    }

    #[test]
    fn test_move_generation_20() {
        let state = TablutState::from_fen("9/9/9/9/9/9/4p4/9/9/ww").unwrap();

        let moves = state.moves_for_player();
        assert!(moves.is_none());
    }

    #[test]
    fn test_move_generation_21() {
        let state = TablutState::from_fen("9/9/9/9/9/9/4p4/9/9/bw").unwrap();

        let moves = state.moves_for_player();
        assert!(moves.is_none());
    }

    fn apply_capture_test(
        m: &Move,
        initial_state: &str,
        expected_state: &str,
        expected_captures: &[usize],
        player: PlayerSide,
    ) {
        let state = TablutState::from_fen(initial_state).unwrap();

        let captures = if player == PlayerSide::Defender {
            state.get_captures_for_white(&m)
        } else {
            state.get_captures_for_black(&m)
        };

        let mut expected = EnumeratingBoardPositionType::ZERO;

        for &capt in expected_captures {
            expected.set(capt, true);
        }

        assert_eq!(expected, captures);

        let new_state_expected = TablutState::from_fen(expected_state).unwrap();
        let new_state_gen = state.apply_move(&m);
        assert_eq!(Ok(new_state_expected), new_state_gen);
    }

    #[test]
    fn test_move_application_for_white_base_from_right() {
        apply_capture_test(
            &Move::new(39, 21),
            "9/9/1Pp6/9/3P5/9/9/9/8p/w",
            "9/9/1P1P5/9/9/9/9/9/8p/b",
            &[20],
            PlayerSide::Defender,
        )
    }

    #[test]
    fn test_move_application_for_white_base_from_below() {
        apply_capture_test(
            &Move::new(48, 39),
            "9/9/3P5/3p5/9/3P5/9/9/8p/w",
            "9/9/3P5/9/3P5/9/9/9/8p/b",
            &[30],
            PlayerSide::Defender,
        );
    }

    #[test]
    fn test_move_application_for_white_base_from_left() {
        apply_capture_test(
            &Move::new(47, 20),
            "9/9/3pP4/9/9/2P6/9/9/8p/w",
            "9/9/2P1P4/9/9/9/9/9/8p/b",
            &[21],
            PlayerSide::Defender,
        );
    }

    #[test]
    fn test_move_application_for_white_base_from_above() {
        apply_capture_test(
            &Move::new(21, 30),
            "9/9/3P5/9/3p5/3P5/9/9/8p/w",
            "9/9/9/3P5/9/3P5/9/9/8p/b",
            &[39],
            PlayerSide::Defender,
        );
    }

    #[test]
    fn test_move_application_for_white_base_against_camp() {
        apply_capture_test(
            &Move::new(48, 39),
            "9/9/9/9/2p6/3P5/9/9/8p/w",
            "9/9/9/9/3P5/9/9/9/8p/b",
            &[38],
            PlayerSide::Defender,
        );
    }

    #[test]
    fn test_move_application_for_white_base_against_throne() {
        apply_capture_test(
            &Move::new(47, 38),
            "9/9/9/9/3p5/2P6/9/9/8p/w",
            "9/9/9/9/2P6/9/9/9/8p/b",
            &[39],
            PlayerSide::Defender,
        );
    }

    #[test]
    fn test_move_application_for_king_base_from_right() {
        apply_capture_test(
            &Move::new(39, 21),
            "9/9/1Pp6/9/3K5/9/9/9/8p/w",
            "9/9/1P1K5/9/9/9/9/9/8p/b",
            &[20],
            PlayerSide::Defender,
        )
    }

    #[test]
    fn test_move_application_for_king_base_from_below() {
        apply_capture_test(
            &Move::new(48, 39),
            "9/9/3P5/3p5/9/3K5/9/9/8p/w",
            "9/9/3P5/9/3K5/9/9/9/8p/b",
            &[30],
            PlayerSide::Defender,
        );
    }

    #[test]
    fn test_move_application_for_king_base_from_left() {
        apply_capture_test(
            &Move::new(47, 20),
            "9/9/3pP4/9/9/2K6/9/9/8p/w",
            "9/9/2K1P4/9/9/9/9/9/8p/b",
            &[21],
            PlayerSide::Defender,
        );
    }

    #[test]
    fn test_move_application_for_king_base_from_above() {
        apply_capture_test(
            &Move::new(21, 30),
            "9/9/3K5/9/3p5/3P5/9/9/8p/w",
            "9/9/9/3K5/9/3P5/9/9/8p/b",
            &[39],
            PlayerSide::Defender,
        );
    }

    #[test]
    fn test_move_application_for_king_base_against_camp() {
        apply_capture_test(
            &Move::new(48, 39),
            "9/9/9/9/2p6/3K5/9/9/8p/w",
            "9/9/9/9/3K5/9/9/9/8p/b",
            &[38],
            PlayerSide::Defender,
        );
    }

    #[test]
    fn test_move_application_for_king_safe_cell() {
        apply_capture_test(
            &Move::new(47, 2),
            "9/9/9/9/9/2K6/9/9/8p/w",
            "2K6/9/9/9/9/9/9/9/8p/ww",
            &[],
            PlayerSide::Defender,
        );
    }

    #[test]
    fn test_move_application_for_king_base_against_throne() {
        apply_capture_test(
            &Move::new(47, 38),
            "9/9/9/9/3p5/2K6/9/9/8p/w",
            "9/9/9/9/2K6/9/9/9/8p/b",
            &[39],
            PlayerSide::Defender,
        );
    }

    #[test]
    fn test_move_application_for_black_base_from_right() {
        apply_capture_test(
            &Move::new(39, 21),
            "9/9/1pP6/9/3p5/9/9/9/8K/b",
            "9/9/1p1p5/9/9/9/9/9/8K/w",
            &[20],
            PlayerSide::Attacker,
        )
    }

    #[test]
    fn test_move_application_for_black_base_from_below() {
        apply_capture_test(
            &Move::new(48, 39),
            "9/9/3p5/3P5/9/3p5/9/9/8K/b",
            "9/9/3p5/9/3p5/9/9/9/8K/w",
            &[30],
            PlayerSide::Attacker,
        );
    }

    #[test]
    fn test_move_application_for_black_base_from_left() {
        apply_capture_test(
            &Move::new(47, 20),
            "9/9/3Pp4/9/9/2p6/9/9/8K/b",
            "9/9/2p1p4/9/9/9/9/9/8K/w",
            &[21],
            PlayerSide::Attacker,
        );
    }

    #[test]
    fn test_move_application_for_black_base_from_above() {
        apply_capture_test(
            &Move::new(21, 30),
            "9/9/3p5/9/3P5/3p5/9/9/8K/b",
            "9/9/9/3p5/9/3p5/9/9/8K/w",
            &[39],
            PlayerSide::Attacker,
        );
    }

    #[test]
    fn test_move_application_for_black_base_against_camp() {
        apply_capture_test(
            &Move::new(48, 39),
            "9/9/9/9/2P6/3p5/9/9/8K/b",
            "9/9/9/9/3p5/9/9/9/8K/w",
            &[38],
            PlayerSide::Attacker,
        );
    }

    #[test]
    fn test_move_application_for_black_base_against_throne() {
        apply_capture_test(
            &Move::new(47, 38),
            "9/9/9/9/3P5/2p6/9/9/8K/b",
            "9/9/9/9/2p6/9/9/9/8K/w",
            &[39],
            PlayerSide::Attacker,
        );
    }

    #[test]
    fn test_move_application_for_black_king_in_castle_from_above() {
        apply_capture_test(
            &Move::new(58, 49),
            "9/9/9/4p4/3pKp3/9/4p4/9/8P/b",
            "9/9/9/4p4/3p1p3/4p4/9/9/8P/bw",
            &[40],
            PlayerSide::Attacker,
        );
    }

    #[test]
    fn test_move_application_for_black_king_in_castle_from_below() {
        apply_capture_test(
            &Move::new(22, 31),
            "9/9/4p4/9/3pKp3/4p4/9/9/8P/b",
            "9/9/9/4p4/3p1p3/4p4/9/9/8P/bw",
            &[40],
            PlayerSide::Attacker,
        );
    }

    #[test]
    fn test_move_application_for_black_king_in_castle_from_right() {
        apply_capture_test(
            &Move::new(42, 41),
            "9/9/9/4p4/3pK1p2/4p4/9/9/8P/b",
            "9/9/9/4p4/3p1p3/4p4/9/9/8P/bw",
            &[40],
            PlayerSide::Attacker,
        );
    }

    #[test]
    fn test_move_application_for_black_king_in_castle_from_left() {
        apply_capture_test(
            &Move::new(38, 39),
            "9/9/9/4p4/2p1Kp3/4p4/9/9/8P/b",
            "9/9/9/4p4/3p1p3/4p4/9/9/8P/bw",
            &[40],
            PlayerSide::Attacker,
        );
    }

    #[test]
    fn test_move_application_for_black_king_above_castle() {
        apply_capture_test(
            &Move::new(33, 32),
            "9/9/4p4/3pK1p2/9/9/9/9/8P/b",
            "9/9/4p4/3p1p3/9/9/9/9/8P/bw",
            &[31],
            PlayerSide::Attacker,
        );
    }

    #[test]
    fn test_move_application_for_black_king_below_castle() {
        apply_capture_test(
            &Move::new(67, 58),
            "9/9/9/9/9/3pKp3/9/4p4/8P/b",
            "9/9/9/9/9/3p1p3/4p4/9/8P/bw",
            &[49],
            PlayerSide::Attacker,
        );
    }

    #[test]
    fn test_move_application_for_black_king_right_of_castle() {
        apply_capture_test(
            &Move::new(23, 32),
            "9/9/5p3/9/5Kp2/5p3/9/9/8P/b",
            "9/9/9/5p3/6p2/5p3/9/9/8P/bw",
            &[41],
            PlayerSide::Attacker,
        );
    }

    #[test]
    fn test_move_application_for_black_king_left_of_castle() {
        apply_capture_test(
            &Move::new(21, 30),
            "9/9/3p5/9/2pK5/3p5/9/9/8P/b",
            "9/9/9/3p5/2p6/3p5/9/9/8P/bw",
            &[39],
            PlayerSide::Attacker,
        );
    }

    #[test]
    fn test_move_application_with_wrong_state() {
        let m = Move::new(21, 30);
        let state = TablutState::from_fen("9/9/3p5/9/2pP5/3p5/9/9/8P/bw")
            .unwrap()
            .apply_move(&m);
        assert_eq!(Err(MoveApplicationError::WrongTurn), state);

        let state = TablutState::from_fen("9/9/3p5/9/2pP5/3p5/9/9/8P/ww")
            .unwrap()
            .apply_move(&m);
        assert_eq!(Err(MoveApplicationError::WrongTurn), state);

        let state = TablutState::from_fen("9/9/3p5/9/2pP5/3p5/9/9/8P/d")
            .unwrap()
            .apply_move(&m);
        assert_eq!(Err(MoveApplicationError::WrongTurn), state);
    }
}
