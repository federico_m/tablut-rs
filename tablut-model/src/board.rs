use bitvec::{array::BitArray, bitarr, prelude::Lsb0, BitArr};

/// Type for describing the board
pub type EnumeratingBoardPositionType = BitArr!(for SIDE_LENGTH * SIDE_LENGTH);

/// Side length of the board
pub const SIDE_LENGTH: usize = 9;

/// Number of black camps
pub const NUM_BLACK_CAMPS: usize = 16;

/// Number of safe cells
pub const NUM_SAFE_CELLS: usize = 8;

/// Castle position
pub const CASTLE_POSITION: EnumeratingBoardPositionType = bitarr![const
    0,0,0,0,0,0,0,0,0,
    0,0,0,0,0,0,0,0,0,
    0,0,0,0,0,0,0,0,0,
    0,0,0,0,0,0,0,0,0,
    0,0,0,0,1,0,0,0,0,
    0,0,0,0,0,0,0,0,0,
    0,0,0,0,0,0,0,0,0,
    0,0,0,0,0,0,0,0,0,
    0,0,0,0,0,0,0,0,0
];

/// Position of all black camps
pub const BLACK_CAMPS_POSITION: EnumeratingBoardPositionType = bitarr![const
    0,0,0,1,1,1,0,0,0,
    0,0,0,0,1,0,0,0,0,
    0,0,0,0,0,0,0,0,0,
    1,0,0,0,0,0,0,0,1,
    1,1,0,0,0,0,0,1,1,
    1,0,0,0,0,0,0,0,1,
    0,0,0,0,0,0,0,0,0,
    0,0,0,0,1,0,0,0,0,
    0,0,0,1,1,1,0,0,0
];

/// West black camp position
pub const BLACK_WEST_CAMPS_POSITION: EnumeratingBoardPositionType = bitarr![const
    0,0,0,0,0,0,0,0,0,
    0,0,0,0,0,0,0,0,0,
    0,0,0,0,0,0,0,0,0,
    1,0,0,0,0,0,0,0,0,
    1,1,0,0,0,0,0,0,0,
    1,0,0,0,0,0,0,0,0,
    0,0,0,0,0,0,0,0,0,
    0,0,0,0,0,0,0,0,0,
    0,0,0,0,0,0,0,0,0
];

/// East black camp position
pub const BLACK_EAST_CAMPS_POSITION: EnumeratingBoardPositionType = bitarr![const
    0,0,0,0,0,0,0,0,0,
    0,0,0,0,0,0,0,0,0,
    0,0,0,0,0,0,0,0,0,
    0,0,0,0,0,0,0,0,1,
    0,0,0,0,0,0,0,1,1,
    0,0,0,0,0,0,0,0,1,
    0,0,0,0,0,0,0,0,0,
    0,0,0,0,0,0,0,0,0,
    0,0,0,0,0,0,0,0,0
];

// North black camp position
pub const BLACK_NORTH_CAMPS_POSITION: EnumeratingBoardPositionType = bitarr![const
    0,0,0,1,1,1,0,0,0,
    0,0,0,0,1,0,0,0,0,
    0,0,0,0,0,0,0,0,0,
    0,0,0,0,0,0,0,0,0,
    0,0,0,0,0,0,0,0,0,
    0,0,0,0,0,0,0,0,0,
    0,0,0,0,0,0,0,0,0,
    0,0,0,0,0,0,0,0,0,
    0,0,0,0,0,0,0,0,0
];

// South black camp position
pub const BLACK_SOUTH_CAMPS_POSITION: EnumeratingBoardPositionType = bitarr![const
    0,0,0,0,0,0,0,0,0,
    0,0,0,0,0,0,0,0,0,
    0,0,0,0,0,0,0,0,0,
    0,0,0,0,0,0,0,0,0,
    0,0,0,0,0,0,0,0,0,
    0,0,0,0,0,0,0,0,0,
    0,0,0,0,0,0,0,0,0,
    0,0,0,0,1,0,0,0,0,
    0,0,0,1,1,1,0,0,0
];

// Position of safe cells
pub const SAFE_CELL_POSITION: EnumeratingBoardPositionType = bitarr![const
    0,1,1,0,0,0,1,1,0,
    1,0,0,0,0,0,0,0,1,
    1,0,0,0,0,0,0,0,1,
    0,0,0,0,0,0,0,0,0,
    0,0,0,0,0,0,0,0,0,
    0,0,0,0,0,0,0,0,0,
    1,0,0,0,0,0,0,0,1,
    1,0,0,0,0,0,0,0,1,
    0,1,1,0,0,0,1,1,0
];

// Starting position for white pawns
pub const STARTING_WHITE_POSITION: EnumeratingBoardPositionType = bitarr![const
    0,0,0,0,0,0,0,0,0,
    0,0,0,0,0,0,0,0,0,
    0,0,0,0,1,0,0,0,0,
    0,0,0,0,1,0,0,0,0,
    0,0,1,1,0,1,1,0,0,
    0,0,0,0,1,0,0,0,0,
    0,0,0,0,1,0,0,0,0,
    0,0,0,0,0,0,0,0,0,
    0,0,0,0,0,0,0,0,0
];

// Starting position for black pawns
pub const STARTING_BLACK_POSITION: EnumeratingBoardPositionType = bitarr![const
    0,0,0,1,1,1,0,0,0,
    0,0,0,0,1,0,0,0,0,
    0,0,0,0,0,0,0,0,0,
    1,0,0,0,0,0,0,0,1,
    1,1,0,0,0,0,0,1,1,
    1,0,0,0,0,0,0,0,1,
    0,0,0,0,0,0,0,0,0,
    0,0,0,0,1,0,0,0,0,
    0,0,0,1,1,1,0,0,0
];

// Starting position for king
pub const STARTING_KING_POSITION: EnumeratingBoardPositionType = bitarr![const
    0,0,0,0,0,0,0,0,0,
    0,0,0,0,0,0,0,0,0,
    0,0,0,0,0,0,0,0,0,
    0,0,0,0,0,0,0,0,0,
    0,0,0,0,1,0,0,0,0,
    0,0,0,0,0,0,0,0,0,
    0,0,0,0,0,0,0,0,0,
    0,0,0,0,0,0,0,0,0,
    0,0,0,0,0,0,0,0,0
];

// Special cells for king
pub const KING_SPECIAL_CELLS: EnumeratingBoardPositionType = bitarr![const
    0,0,0,0,0,0,0,0,0,
    0,0,0,0,0,0,0,0,0,
    0,0,0,0,0,0,0,0,0,
    0,0,0,0,1,0,0,0,0,
    0,0,0,1,1,1,0,0,0,
    0,0,0,0,1,0,0,0,0,
    0,0,0,0,0,0,0,0,0,
    0,0,0,0,0,0,0,0,0,
    0,0,0,0,0,0,0,0,0
];

// Special state in which the king is surrounded from all sides
pub const KING_SURROUNDED_CELLS: EnumeratingBoardPositionType = bitarr![const
    0,0,0,0,0,0,0,0,0,
    0,0,0,0,0,0,0,0,0,
    0,0,0,0,0,0,0,0,0,
    0,0,0,0,1,0,0,0,0,
    0,0,0,1,0,1,0,0,0,
    0,0,0,0,1,0,0,0,0,
    0,0,0,0,0,0,0,0,0,
    0,0,0,0,0,0,0,0,0,
    0,0,0,0,0,0,0,0,0
];

// Special state in which king is surrounded from above
pub const KING_SURROUNDED_ABOVE_CELLS: EnumeratingBoardPositionType = bitarr![const
    0,0,0,0,0,0,0,0,0,
    0,0,0,0,0,0,0,0,0,
    0,0,0,0,1,0,0,0,0,
    0,0,0,1,0,1,0,0,0,
    0,0,0,0,0,0,0,0,0,
    0,0,0,0,0,0,0,0,0,
    0,0,0,0,0,0,0,0,0,
    0,0,0,0,0,0,0,0,0,
    0,0,0,0,0,0,0,0,0
];

// Special state in which king is surrounded from below
pub const KING_SURROUNDED_BELOW_CELLS: EnumeratingBoardPositionType = bitarr![const
    0,0,0,0,0,0,0,0,0,
    0,0,0,0,0,0,0,0,0,
    0,0,0,0,0,0,0,0,0,
    0,0,0,0,0,0,0,0,0,
    0,0,0,0,0,0,0,0,0,
    0,0,0,1,0,1,0,0,0,
    0,0,0,0,1,0,0,0,0,
    0,0,0,0,0,0,0,0,0,
    0,0,0,0,0,0,0,0,0
];

// Special state in which king is surrounded from right
pub const KING_SURROUNDED_RIGHT_CELLS: EnumeratingBoardPositionType = bitarr![const
    0,0,0,0,0,0,0,0,0,
    0,0,0,0,0,0,0,0,0,
    0,0,0,0,0,0,0,0,0,
    0,0,0,0,0,1,0,0,0,
    0,0,0,0,0,0,1,0,0,
    0,0,0,0,0,1,0,0,0,
    0,0,0,0,0,0,0,0,0,
    0,0,0,0,0,0,0,0,0,
    0,0,0,0,0,0,0,0,0
];

// Special state in which king is surrounded from left
pub const KING_SURROUNDED_LEFT_CELLS: EnumeratingBoardPositionType = bitarr![const
    0,0,0,0,0,0,0,0,0,
    0,0,0,0,0,0,0,0,0,
    0,0,0,0,0,0,0,0,0,
    0,0,0,1,0,0,0,0,0,
    0,0,1,0,0,0,0,0,0,
    0,0,0,1,0,0,0,0,0,
    0,0,0,0,0,0,0,0,0,
    0,0,0,0,0,0,0,0,0,
    0,0,0,0,0,0,0,0,0
];

// Map containing all cells considered obstacles
pub const OBSTACLES_CELLS: EnumeratingBoardPositionType = bitarr![const
    0,0,0,1,1,1,0,0,0,
    0,0,0,0,1,0,0,0,0,
    0,0,0,0,0,0,0,0,0,
    1,0,0,0,0,0,0,0,1,
    1,1,0,0,1,0,0,1,1,
    1,0,0,0,0,0,0,0,1,
    0,0,0,0,0,0,0,0,0,
    0,0,0,0,1,0,0,0,0,
    0,0,0,1,1,1,0,0,0
];

// Tests if a black pawn is inside one of the camps
#[inline]
pub fn black_pawn_is_in_camp(idx: usize) -> bool {
    black_pawn_is_in_west_camp(idx)
        || black_pawn_is_in_east_camp(idx)
        || black_pawn_is_in_north_camp(idx)
        || black_pawn_is_in_south_camp(idx)
}

// Tests if a black pawn is inside west camp
#[inline]
pub fn black_pawn_is_in_west_camp(idx: usize) -> bool {
    let mut mask: EnumeratingBoardPositionType = BitArray::ZERO;
    mask.set(idx, true);

    (mask & BLACK_WEST_CAMPS_POSITION).count_ones() > 0
}

// Tests if a black pawn is inside east camp
#[inline]
pub fn black_pawn_is_in_east_camp(idx: usize) -> bool {
    let mut mask: EnumeratingBoardPositionType = BitArray::ZERO;
    mask.set(idx, true);

    (mask & BLACK_EAST_CAMPS_POSITION).count_ones() > 0
}

// Tests if a black pawn is inside north camp
#[inline]
pub fn black_pawn_is_in_north_camp(idx: usize) -> bool {
    let mut mask: EnumeratingBoardPositionType = BitArray::ZERO;
    mask.set(idx, true);

    (mask & BLACK_NORTH_CAMPS_POSITION).count_ones() > 0
}

// Tests if a black pawn is inside south camp
#[inline]
pub fn black_pawn_is_in_south_camp(idx: usize) -> bool {
    let mut mask: EnumeratingBoardPositionType = BitArray::ZERO;
    mask.set(idx, true);

    (mask & BLACK_SOUTH_CAMPS_POSITION).count_ones() > 0
}

// Get the first cell for a row
#[inline]
pub fn first_cell_for_row(cell: usize) -> usize {
    cell - board_column_idx(cell)
}

// Get the last cell for a row
#[inline]
pub fn last_cell_for_row(cell: usize) -> usize {
    first_cell_for_row(cell) + SIDE_LENGTH - 1
}

// Gets whether the cell is in the first row
#[inline]
pub fn is_first_row(cell: usize) -> bool {
    board_row_idx(cell) == 0
}

// Get whether the cell is in the last row
#[inline]
pub fn is_last_row(cell: usize) -> bool {
    board_row_idx(cell) == SIDE_LENGTH - 1
}

// Get the first cell for a column
#[inline]
pub fn first_cell_for_column(cell: usize) -> usize {
    board_column_idx(cell)
}

// Get the last cell for a column
#[inline]
pub fn last_cell_for_column(cell: usize) -> usize {
    SIDE_LENGTH * (SIDE_LENGTH - 1) + board_column_idx(cell)
}

// Gets whether the cell is in the first column
#[inline]
pub fn is_first_column(cell: usize) -> bool {
    board_column_idx(cell) == 0
}

// Gets whether the cell is in the last column
#[inline]
pub fn is_last_column(cell: usize) -> bool {
    board_column_idx(cell) == SIDE_LENGTH - 1
}

// Gets the index of the cell above the one considered
#[inline]
pub fn one_cell_up(cell: usize) -> Option<usize> {
    if is_first_row(cell) {
        return None;
    }

    Some(cell - SIDE_LENGTH)
}

// Gets the index of the cell below the one considered
#[inline]
pub fn one_cell_down(cell: usize) -> Option<usize> {
    if is_last_row(cell) {
        return None;
    }

    Some(cell + SIDE_LENGTH)
}

// Gets the index of the cell right of the one considered
#[inline]
pub fn one_cell_right(cell: usize) -> Option<usize> {
    if is_last_column(cell) {
        return None;
    }

    Some(cell + 1)
}

// Gets the index of the cell left of the one considered
#[inline]
pub fn one_cell_left(cell: usize) -> Option<usize> {
    if is_first_column(cell) {
        return None;
    }

    Some(cell - 1)
}

// Gets the row index of a cell
#[inline]
pub fn board_row_idx(cell: usize) -> usize {
    cell / SIDE_LENGTH
}

// Gets the column index of a cell
#[inline]
pub fn board_column_idx(cell: usize) -> usize {
    cell % SIDE_LENGTH
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn pawn_is_in_black_camp() {
        assert!(black_pawn_is_in_camp(3));
        assert!(black_pawn_is_in_camp(4));
        assert!(black_pawn_is_in_camp(5));
        assert!(black_pawn_is_in_camp(13));

        assert!(black_pawn_is_in_camp(27));
        assert!(black_pawn_is_in_camp(36));
        assert!(black_pawn_is_in_camp(45));
        assert!(black_pawn_is_in_camp(37));

        assert!(black_pawn_is_in_camp(35));
        assert!(black_pawn_is_in_camp(44));
        assert!(black_pawn_is_in_camp(53));
        assert!(black_pawn_is_in_camp(43));

        assert!(black_pawn_is_in_camp(75));
        assert!(black_pawn_is_in_camp(76));
        assert!(black_pawn_is_in_camp(77));
        assert!(black_pawn_is_in_camp(67));
    }

    #[test]
    fn pawn_is_in_north_camp() {
        assert!(black_pawn_is_in_north_camp(3));
        assert!(black_pawn_is_in_north_camp(4));
        assert!(black_pawn_is_in_north_camp(5));
        assert!(black_pawn_is_in_north_camp(13));

        assert!(!black_pawn_is_in_north_camp(27));
        assert!(!black_pawn_is_in_north_camp(36));
        assert!(!black_pawn_is_in_north_camp(45));
        assert!(!black_pawn_is_in_north_camp(37));

        assert!(!black_pawn_is_in_north_camp(35));
        assert!(!black_pawn_is_in_north_camp(44));
        assert!(!black_pawn_is_in_north_camp(53));
        assert!(!black_pawn_is_in_north_camp(43));

        assert!(!black_pawn_is_in_north_camp(75));
        assert!(!black_pawn_is_in_north_camp(76));
        assert!(!black_pawn_is_in_north_camp(77));
        assert!(!black_pawn_is_in_north_camp(67));
    }

    #[test]
    fn pawn_is_in_east_camp() {
        assert!(!black_pawn_is_in_east_camp(3));
        assert!(!black_pawn_is_in_east_camp(4));
        assert!(!black_pawn_is_in_east_camp(5));
        assert!(!black_pawn_is_in_east_camp(13));

        assert!(!black_pawn_is_in_east_camp(27));
        assert!(!black_pawn_is_in_east_camp(36));
        assert!(!black_pawn_is_in_east_camp(45));
        assert!(!black_pawn_is_in_east_camp(37));

        assert!(black_pawn_is_in_east_camp(35));
        assert!(black_pawn_is_in_east_camp(44));
        assert!(black_pawn_is_in_east_camp(53));
        assert!(black_pawn_is_in_east_camp(43));

        assert!(!black_pawn_is_in_east_camp(75));
        assert!(!black_pawn_is_in_east_camp(76));
        assert!(!black_pawn_is_in_east_camp(77));
        assert!(!black_pawn_is_in_east_camp(67));
    }

    #[test]
    fn pawn_is_in_west_camp() {
        assert!(!black_pawn_is_in_west_camp(3));
        assert!(!black_pawn_is_in_west_camp(4));
        assert!(!black_pawn_is_in_west_camp(5));
        assert!(!black_pawn_is_in_west_camp(13));

        assert!(black_pawn_is_in_west_camp(27));
        assert!(black_pawn_is_in_west_camp(36));
        assert!(black_pawn_is_in_west_camp(45));
        assert!(black_pawn_is_in_west_camp(37));

        assert!(!black_pawn_is_in_west_camp(35));
        assert!(!black_pawn_is_in_west_camp(44));
        assert!(!black_pawn_is_in_west_camp(53));
        assert!(!black_pawn_is_in_west_camp(43));

        assert!(!black_pawn_is_in_west_camp(75));
        assert!(!black_pawn_is_in_west_camp(76));
        assert!(!black_pawn_is_in_west_camp(77));
        assert!(!black_pawn_is_in_west_camp(67));
    }

    #[test]
    fn pawn_is_in_south_camp() {
        assert!(!black_pawn_is_in_south_camp(3));
        assert!(!black_pawn_is_in_south_camp(4));
        assert!(!black_pawn_is_in_south_camp(5));
        assert!(!black_pawn_is_in_south_camp(13));

        assert!(!black_pawn_is_in_south_camp(27));
        assert!(!black_pawn_is_in_south_camp(36));
        assert!(!black_pawn_is_in_south_camp(45));
        assert!(!black_pawn_is_in_south_camp(37));

        assert!(!black_pawn_is_in_south_camp(35));
        assert!(!black_pawn_is_in_south_camp(44));
        assert!(!black_pawn_is_in_south_camp(53));
        assert!(!black_pawn_is_in_south_camp(43));

        assert!(black_pawn_is_in_south_camp(75));
        assert!(black_pawn_is_in_south_camp(76));
        assert!(black_pawn_is_in_south_camp(77));
        assert!(black_pawn_is_in_south_camp(67));
    }

    #[test]
    fn first_last_of_all() {
        assert!(is_first_row(0));
        assert!(is_first_column(0));
        for i in 1..SIDE_LENGTH {
            assert!(!is_first_row(i * 9));
            assert!(!is_first_column(i));
        }

        assert!(is_last_row(SIDE_LENGTH * SIDE_LENGTH - 1));
        assert!(is_last_column(SIDE_LENGTH - 1));
        for i in 0..SIDE_LENGTH - 1 {
            assert!(!is_last_row(i * 9));
            assert!(!is_last_column(i));
        }
    }

    #[test]
    fn test_first_cell_for_row() {
        assert_eq!(first_cell_for_row(1), 0);
        assert_eq!(first_cell_for_row(10), 9);
        assert_eq!(first_cell_for_row(19), 18);
        assert_eq!(first_cell_for_row(79), 72);
    }

    #[test]
    fn test_last_cell_for_row() {
        assert_eq!(last_cell_for_row(1), 8);
        assert_eq!(last_cell_for_row(10), 17);
        assert_eq!(last_cell_for_row(19), 26);
        assert_eq!(last_cell_for_row(79), 80);
    }

    #[test]
    fn test_first_cell_for_column() {
        assert_eq!(first_cell_for_column(1), 1);
        assert_eq!(first_cell_for_column(12), 3);
        assert_eq!(first_cell_for_column(20), 2);
        assert_eq!(first_cell_for_column(79), 7);
    }

    #[test]
    fn test_last_cell_for_column() {
        assert_eq!(last_cell_for_column(1), 73);
        assert_eq!(last_cell_for_column(12), 75);
        assert_eq!(last_cell_for_column(20), 74);
        assert_eq!(last_cell_for_column(79), 79);
    }

    #[test]
    fn test_one_cell_up() {
        assert_eq!(one_cell_up(1), None);
        assert_eq!(one_cell_up(10), Some(1));
        assert_eq!(one_cell_up(19), Some(10));
        assert_eq!(one_cell_up(79), Some(70));
    }

    #[test]
    fn test_one_cell_down() {
        assert_eq!(one_cell_down(1), Some(10));
        assert_eq!(one_cell_down(10), Some(19));
        assert_eq!(one_cell_down(19), Some(28));
        assert_eq!(one_cell_down(79), None);
    }

    #[test]
    fn test_one_cell_left() {
        assert_eq!(one_cell_left(0), None);
        assert_eq!(one_cell_left(9), None);
        assert_eq!(one_cell_left(19), Some(18));
        assert_eq!(one_cell_left(79), Some(78));
    }

    #[test]
    fn test_one_cell_right() {
        assert_eq!(one_cell_right(0), Some(1));
        assert_eq!(one_cell_right(9), Some(10));
        assert_eq!(one_cell_right(17), None);
        assert_eq!(one_cell_right(80), None);
    }

    #[test]
    fn test_row_idx() {
        assert_eq!(0, board_row_idx(1));
        assert_eq!(1, board_row_idx(11));
        assert_eq!(2, board_row_idx(21));
        assert_eq!(3, board_row_idx(31));
        assert_eq!(8, board_row_idx(80));
    }

    #[test]
    fn test_column_idx() {
        assert_eq!(1, board_column_idx(1));
        assert_eq!(2, board_column_idx(11));
        assert_eq!(3, board_column_idx(21));
        assert_eq!(4, board_column_idx(31));
        assert_eq!(8, board_column_idx(80));
    }
}
