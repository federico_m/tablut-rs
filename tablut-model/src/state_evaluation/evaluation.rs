use crate::state::TablutState;

pub trait StateEvaluator {
    // Evaluates the state to get it's heuristic value
    fn evaluate_state(&self, state: &TablutState) -> i32;
}
