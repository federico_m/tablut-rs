use crate::{model::Turn, state::TablutState};

use super::evaluation::StateEvaluator;

pub struct NaiveEvaluator;

impl StateEvaluator for NaiveEvaluator {
    fn evaluate_state(&self, state: &TablutState) -> i32 {
        match state.turn() {
            Turn::White | Turn::Black => (state.white_positions().count_ones() as i32)
                .wrapping_sub(state.black_positions().count_ones() as i32)
                .wrapping_add(7),
            Turn::WhiteWon => i32::MAX,
            Turn::BlackWon => i32::MIN,
            Turn::Draw => 0,
        }
    }
}

#[cfg(test)]
mod tests {
    use crate::state::TablutState;

    use super::*;

    fn test_evaluate(fen: &str, expected: i32) {
        let state = TablutState::from_fen(fen).unwrap();
        let evaluator = NaiveEvaluator;

        assert_eq!(expected, evaluator.evaluate_state(&state));
    }

    #[test]
    fn basic_test() {
        test_evaluate("3ppp3/4p4/4P4/p3P3p/ppPPKPPpp/p3P3p/4P4/4p4/3ppp3/w", 0);
    }

    #[test]
    fn white_turn_test() {
        test_evaluate("3ppp3/9/9/9/4K4/9/9/9/9/w", 5);
    }

    #[test]
    fn black_turn_test() {
        test_evaluate("3ppp3/9/9/9/4K4/9/9/9/9/b", 5);
    }

    #[test]
    fn white_victory_turn_test() {
        test_evaluate("3ppp3/9/9/9/4K4/9/9/9/9/ww", i32::MAX);
    }

    #[test]
    fn black_victory_turn_test() {
        test_evaluate("3ppp3/9/9/9/4K4/9/9/9/9/bw", i32::MIN);
    }

    #[test]
    fn draw_turn_test() {
        test_evaluate("3ppp3/9/9/9/4K4/9/9/9/9/d", 0);
    }
}
