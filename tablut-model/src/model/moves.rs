use crate::board::{board_column_idx, board_row_idx};

#[derive(Debug, PartialOrd, PartialEq, Ord, Eq)]
pub struct Move {
    pub from: usize,
    pub to: usize,
}

impl Move {
    pub fn new(from: usize, to: usize) -> Self {
        #[cfg(any(test, debug))]
        {
            if board_row_idx(from) != board_row_idx(to)
                && board_column_idx(from) != board_column_idx(to)
            {
                panic!("Moves are only allowed in straight line");
            }
        }

        Move { from, to }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn create_move_ok_row() {
        Move::new(0, 7);
    }

    #[test]
    fn create_move_ok_column() {
        Move::new(1, 28);
    }

    #[test]
    #[should_panic]
    fn create_move_should_panic_1() {
        Move::new(9, 7);
    }

    #[test]
    #[should_panic]
    fn create_move_should_panic_2() {
        Move::new(0, 28);
    }
}
