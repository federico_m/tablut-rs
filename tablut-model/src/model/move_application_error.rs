#[derive(thiserror::Error, Debug, Clone, PartialEq, Eq)]
pub enum MoveApplicationError {
    #[error("Cannot apply a move to a state with the wrong turn")]
    WrongTurn,
}
