#[derive(Debug, PartialEq, PartialOrd, Ord, Eq)]
pub struct Coord {
    row: u8,
    col: u8,
}
