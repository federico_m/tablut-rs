#[derive(thiserror::Error, Debug, Clone, PartialEq, Eq)]
pub enum FenErrors {
    #[error("Source is empty")]
    EmptySource,
    #[error("Could not parse")]
    InvalidParse,
    #[error("Parsing produced the wrong number of parts")]
    InvalidNumberOfParts,
    #[error("One or more tags describing the board contains at least one invalid character")]
    InvalidBoardTag,
    #[error("Invalid turn contained")]
    InvalidTurn,
    #[error("Column is too wide")]
    ColumnTooWide,
}
