#[derive(Debug, PartialEq, PartialOrd, Ord, Eq, Clone, Copy)]
pub enum Turn {
    White,
    Black,
    WhiteWon,
    BlackWon,
    Draw,
}
