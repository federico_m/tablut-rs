#[derive(Debug, PartialEq, PartialOrd, Ord, Eq)]
pub enum PlayerSide {
    Attacker,
    Defender,
}
