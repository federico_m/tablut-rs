[[_TOC_]]

# Tablut-rs

A Rust player for Tablut using Ashton rules.

This repo provides a player and a server to which players can connect.

## Ashton Rules

There are two players playing on a 9x9 grid. Attackers (aka black) have 16 knights, while defenders (aka white) have 8 knights and a king to protect.

The two players have two distinct goals:

* Attackers aim to capture the king
* Defenders aim to help the king flee to one of the safe cells

Pieces can only move in an orthogonally (like the rook in chess). They can move as many squares as they want, but they cannot climb over other pawns nor obstacles (the castle and the attacker camps). The only two exceptions to this rule are:
* Black knights can move inside their camp as long as they do not exit from it. Once they exit they cannot get back inside
* The king starts inside the castle. Once it exits the castle, though, he cannot get back inside

A piece (knight or king) is considered captured (therefore removed from the board) if it comes surrounded by opponents on 2 opposite sides (it's possible to capture more than one piece at a time).
There are four special captures:
* If the king is inside the castle, he must be surrounded from all sides
* If the king is asjacent to the castle, he must be surrounded on the three free sides
* If a knight is adjacent to the castle, he can be captured by a single opponent piece positioned on the opposite side of the castle (it doesn't matter whether the king is inside the castle or not)
* If a piece (kinght or king) is asjacent to a camp it can be captured by a single piece positioned on the opposite side of the camp (it doesn't matter whether a black knight is inside the camp or not)

All captures must be active, taht is if a piece moves between two opponent pieces it is not considered captured.

White always begin first.

A game is considered terminated when one of the following condition is met:
* The king manages to flee to one of the safe cells, leading to a white victory
* Black knights capture the king, leading to a black victory
* A player cannot make any move, leading to its defeat
* A board state is reached twice, leading to a draw

## State evaluation

There is currently only one state evaluation technique. 

### Naive
This is a simple and naive technique. Each state's value is computed as follows:

$$
v = |W| + |K| - |B|\\

-16\le v \le 9
$$

where:
* $|W|$ denotes the number of white knights
* $|K|$ denotes the number of kings ($0$ or $1$)
* $|B|$ denotes the number of black knights




# Building the project

The game can be compiled using:
```bash 
cargo build --release
```

# Notes

Current todo list:

- Game
  - [x] Naive move generation
  - [X] Naive move application
  - [X] Naive state evaluation
  - [ ] Optimised state evaluation
  - [ ] Optimising move generation
  - [ ] Optimising move application
  - [ ] Optimising search in move space
- Players/server
  - [ ] Creating players
  - [ ] Creating the server to play
  - [ ] Generating APIs to communicate with the server
- Training
  - [ ] Algorithm for choosing the weights for optimised move evaluation
  - [ ] Training